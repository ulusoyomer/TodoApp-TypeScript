type Method = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH';

function returnCorrectRequest(method: Method, data: unknown = {}): RequestInit {
	switch (method) {
		case 'GET':
			return {
				method,
				headers: {
					'Content-Type': 'application/json',
				},
			};
		case 'POST':
		case 'PUT':
		case 'DELETE':
		case 'PATCH':
			return {
				method,
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(data),
			};
		default:
			return {
				method,
				headers: {
					'Content-Type': 'application/json',
				},
			};
	}
}
export async function sendApiRequest<T>(
	url: string,
	method: Method,
	data: unknown = {},
): Promise<T> {
	const response = await fetch(url, returnCorrectRequest(method, data));

	if (!response.ok) {
		throw new Error(response.statusText);
	}

	return (await response.json()) as Promise<T>;
}
