import React, { FC, ReactElement, useContext, useEffect } from 'react';
import { Grid, Box, Alert, LinearProgress } from '@mui/material';
import TaskCounter from '../taskCounter/TaskCounter';
import Task from '../task/Task';
import { Status } from '../createTaskForm/enums/Status';
import { useQuery, useMutation } from '@tanstack/react-query';
import { sendApiRequest } from '../../helpers/sendApiRequest';
import { ITaskApi } from './interfaces/ITaskApi';
import { IUpdateTask } from '../createTaskForm/interfaces/IUpdateTask';
import { countTask } from './helpers/countTask';
import { TaskStatusChangedContext } from '../../context';

import dayjs from 'dayjs';
import 'dayjs/locale/tr';
dayjs.locale('tr');

const TaskArea: FC = (): ReactElement => {
	const tasksUpdatedContext = useContext(TaskStatusChangedContext);

	const { data, isLoading, isError, refetch } = useQuery(
		['tasks'],
		async () => {
			return await sendApiRequest<ITaskApi>('/api', 'GET');
		},
	);

	const updateTaskMutation = useMutation(async (task: IUpdateTask) =>
		sendApiRequest<ITaskApi>(`/api/${task.id}`, 'PATCH', {
			status: task.status,
		}),
	);

	const onStatusChangeHandler = (
		event: React.ChangeEvent<HTMLInputElement>,
		id: string,
	) => {
		updateTaskMutation.mutate({
			id,
			status: event.target.checked ? Status.inProgress : Status.todo,
		});
	};

	const markCompleteHandler = (
		event: React.MouseEvent<HTMLButtonElement | HTMLAnchorElement>,
		id: string,
	) => {
		updateTaskMutation.mutate({
			id,
			status: Status.completed,
		});
	};

	useEffect(() => {
		console.log('tasksUpdatedContext.updated', tasksUpdatedContext.updated);
		refetch();
	}, [tasksUpdatedContext.updated]);

	useEffect(() => {
		console.log('updateTaskMutation.isSuccess', updateTaskMutation.isSuccess);
		if (updateTaskMutation.isSuccess) {
			tasksUpdatedContext.toggle();
		}
	}, [updateTaskMutation.isSuccess]);

	return (
		<Grid item md={8} px={4}>
			<Box marginBottom={10}>
				<h2>
					Yapılacak işlerin durumları ve içeriği{' - '}
					{dayjs().format('DD MMMM YYYY')}
				</h2>
			</Box>
			<Grid container display={'flex'} justifyContent={'center'}>
				<Grid
					item
					display={'flex'}
					flexDirection={'row'}
					justifyContent={'space-between'}
					alignItems={'center'}
					md={10}
					xs={12}
					mb={8}
				>
					<TaskCounter
						status={Status.todo}
						count={data ? countTask(data, Status.todo) : undefined}
					/>
					<TaskCounter
						status={Status.inProgress}
						count={data ? countTask(data, Status.inProgress) : undefined}
					/>
					<TaskCounter
						status={Status.completed}
						count={data ? countTask(data, Status.completed) : undefined}
					/>
				</Grid>
				<Grid item display={'flex'} flexDirection={'column'} xs={10} mb={8}>
					{isError && (
						<Alert severity="error" style={{ marginBottom: '16px' }}>
							İşler yüklenirken bir hata oluştu. Lütfen daha sonra tekrar
							deneyiniz.
						</Alert>
					)}
					{!isError && Array.isArray(data) && data.length === 0 && (
						<Alert severity="warning" style={{ marginBottom: '16px' }}>
							Hiçbir iş bulunamadı.
						</Alert>
					)}

					{isLoading ? (
						<LinearProgress />
					) : (
						Array.isArray(data) &&
						data.length > 0 && (
							<>
								{data.map((task) => {
									return (
										task.status !== Status.completed && (
											<Task
												key={task.id}
												id={task.id}
												title={task.title}
												description={task.description}
												date={new Date(task.date)}
												status={task.status}
												priority={task.priority}
												onClick={markCompleteHandler}
												onStatusChange={onStatusChangeHandler}
											/>
										)
									);
								})}
							</>
						)
					)}
				</Grid>
			</Grid>
		</Grid>
	);
};
export default TaskArea;
