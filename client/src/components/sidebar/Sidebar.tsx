import { Grid } from '@mui/material';
import { FC, ReactElement } from 'react';
import Profile from '../profile/Profile';
import CreateTaskForm from '../createTaskForm/CreateTaskForm';

const Sidebar: FC = (): ReactElement => {
	return (
		<Grid
			item
			md={4}
			sx={{
				height: '100vh',
				position: 'fixed',
				top: 0,
				right: 0,
				width: '100%',
				backgroundColor: 'background.paper',
				display: 'flex',
				justifyContent: 'center',
				flexDirection: 'column',
				alignItems: 'center',
			}}
		>
			<Profile name="Ömer" />

			<CreateTaskForm />
		</Grid>
	);
};
export default Sidebar;
