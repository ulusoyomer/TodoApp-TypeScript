import { FC, ReactElement } from 'react';
import { Box, Avatar, Typography } from '@mui/material';
import { ITaskCounter } from './interfaces/ITaskCounter';
import { Status } from '../createTaskForm/enums/Status';
import { emitCorrectBorderColor } from './helpers/emitCorrectBorderColor';
import { emitCorrectLabel } from './helpers/emitCorrectLabel';
import PropTypes from 'prop-types';

const TaskCounter: FC<ITaskCounter> = (props): ReactElement => {
	const { status = Status.todo, count = 0 } = props;

	return (
		<>
			<Box
				display="flex"
				flexDirection="column"
				justifyContent="center"
				alignItems="center"
			>
				<Avatar
					sx={{
						backgroundColor: 'transparent',
						border: '5px solid',
						width: '96px',
						height: '96px',
						marginBottom: '16px',
						borderColor: `${emitCorrectBorderColor(status)}`,
					}}
				>
					<Typography variant="h4" color="white">
						{count}
					</Typography>
				</Avatar>
				<Typography
					variant="h5"
					fontSize={'20px'}
					fontWeight={'bold'}
					color="white"
				>
					{emitCorrectLabel(status)}
				</Typography>
			</Box>
		</>
	);
};

TaskCounter.propTypes = {
	status: PropTypes.oneOf([Status.todo, Status.inProgress, Status.completed]),
	count: PropTypes.number,
};

export default TaskCounter;
