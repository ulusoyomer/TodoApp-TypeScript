import { TaskCounterStatusType } from '../interfaces/ITaskCounter';
import { Status } from '../../createTaskForm/enums/Status';

export const emitCorrectLabel = (status: TaskCounterStatusType) => {
	switch (status) {
		case Status.completed:
			return 'Tamamlandı';
		case Status.inProgress:
			return 'Yapım Aşamasında';
		case Status.todo:
			return 'Yapılacak';
		default:
			return 'Unknown';
	}
};
