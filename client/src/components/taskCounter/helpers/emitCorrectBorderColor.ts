import { TaskCounterStatusType } from '../interfaces/ITaskCounter';
import { Status } from '../../createTaskForm/enums/Status';
export const emitCorrectBorderColor = (
	status: TaskCounterStatusType,
): string => {
	switch (status) {
		case Status.completed:
			return 'success.light';
		case Status.inProgress:
			return 'warning.light';
		case Status.todo:
			return 'error.light';
		default:
			return 'black';
	}
};
