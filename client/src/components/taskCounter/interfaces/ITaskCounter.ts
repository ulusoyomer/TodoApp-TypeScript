import { Status } from '../../createTaskForm/enums/Status';

export type TaskCounterStatusType = Status;

export interface ITaskCounter {
	count?: number;
	status?: TaskCounterStatusType;
}
