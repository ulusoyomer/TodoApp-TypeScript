import { FC, ReactElement } from 'react';
import { Avatar, Box, Typography } from '@mui/material';
import PropTypes from 'prop-types';

interface IProfile {
	name?: string;
}

const Profile: FC<IProfile> = (props): ReactElement => {
	const { name = 'John' } = props;
	return (
		<Box
			display={'flex'}
			flexDirection={'column'}
			justifyContent={'center'}
			alignItems={'center'}
		>
			<Avatar
				sx={{
					width: '96px',
					height: '96px',
					backgroundColor: 'primary.main',
					marginBottom: '16px',
				}}
			>
				<Typography variant="h4" color={'text.primary'}>
					{`${name.charAt(0).toUpperCase()}`}
				</Typography>
			</Avatar>
			<Typography variant="h6" color={'text.primary'}>
				Hoşgeldin, {name}
			</Typography>
			<Typography variant="body1" color={'text.primary'}>
				Oluşturduğunuz işleri yönetin.
			</Typography>
		</Box>
	);
};

Profile.propTypes = {
	name: PropTypes.string,
};

export default Profile;
