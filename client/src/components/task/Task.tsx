import { FC, ReactElement } from 'react';
import { Box } from '@mui/material';
import TaskHeader from './_taskHeader';
import TaskDescription from './_taskDescription';
import TaskFooter from './_taskFooter';
import { ITask } from './interfaces/ITask';
import { Status } from '../createTaskForm/enums/Status';
import { Priority } from '../createTaskForm/enums/Priority';
import PropTypes from 'prop-types';
import { PriorityBorderColor } from './helpers/PriorityBorderColor';

const Task: FC<ITask> = (props): ReactElement => {
	const {
		title = 'Task title',
		date = new Date(),
		description = "Task's description",
		onClick = (e) => console.log(e),
		onStatusChange = (e) => console.log(e),
		id = '0',
		priority = Priority.low,
		status = Status.todo,
	} = props;

	return (
		<Box
			display="flex"
			width={'100%'}
			justifyContent={'flex-start'}
			flexDirection={'column'}
			mb={4}
			p={2}
			sx={{
				width: '100%',
				backgroundColor: 'background.paper',
				borderRadius: '8px',
				border: '1px solid',
				borderColor: PriorityBorderColor(priority),
			}}
		>
			<TaskHeader title={title} date={date} />
			<TaskDescription description={description} />
			<TaskFooter
				onClick={onClick}
				onStatusChange={onStatusChange}
				id={id}
				status={status}
			/>
		</Box>
	);
};

Task.propTypes = {
	title: PropTypes.string,
	date: PropTypes.instanceOf(Date),
	description: PropTypes.string,
	onClick: PropTypes.func,
	onStatusChange: PropTypes.func,
	priority: PropTypes.oneOf([Priority.high, Priority.low, Priority.medium]),
	status: PropTypes.oneOf([Status.todo, Status.inProgress, Status.completed]),
	id: PropTypes.string.isRequired,
};

export default Task;
