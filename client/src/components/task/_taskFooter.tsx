import { FC, ReactElement } from 'react';
import { Box, Button, FormControlLabel, Switch } from '@mui/material';
import { Status } from '../createTaskForm/enums/Status';
import { ITaskFooter } from './interfaces/ITaskFooter';
import PropTypes from 'prop-types';

const TaskFooter: FC<ITaskFooter> = (props): ReactElement => {
	const {
		id,
		status,
		onStatusChange = (e) => console.log(e),
		onClick = (e) => console.log(e),
	} = props;
	return (
		<Box
			display={'flex'}
			justifyContent={'space-between'}
			alignItems={'center'}
			mt={4}
		>
			<FormControlLabel
				label={Status.inProgress}
				control={
					<Switch
						color="warning"
						onChange={(e) => onStatusChange(e, id)}
						defaultChecked={status === Status.inProgress ? true : false}
					/>
				}
			/>
			<Button
				variant="contained"
				color="success"
				size="small"
				sx={{ color: '#ffffff' }}
				onClick={(e) => onClick(e, id)}
			>
				Tamamlandı
			</Button>
		</Box>
	);
};

TaskFooter.propTypes = {
	id: PropTypes.string.isRequired,
	status: PropTypes.string.isRequired,
	onStatusChange: PropTypes.func,
	onClick: PropTypes.func,
};

export default TaskFooter;
