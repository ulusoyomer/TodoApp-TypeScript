import { Priority } from '../../createTaskForm/enums/Priority';

export const PriorityBorderColor = (priority: string): string => {
	switch (priority) {
		case Priority.high:
			return 'error.light';
		case Priority.medium:
			return 'info.light';
		case Priority.low:
			return 'white.900';
		default:
			return 'border-gray-500';
	}
};
