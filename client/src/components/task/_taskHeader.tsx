import { FC, ReactElement } from 'react';
import { Box, Chip, Typography } from '@mui/material';
import { ITaskHeader } from './interfaces/ITaskHeader';
import dayjs from 'dayjs';
import 'dayjs/locale/tr';
dayjs.locale('tr');

import PropTypes from 'prop-types';

const TaskHeader: FC<ITaskHeader> = (props): ReactElement => {
	const { title = 'işin başlığı', date = new Date() } = props;
	return (
		<Box
			display="flex"
			width={'100%'}
			justifyContent={'space-between'}
			mb={3}
		>
			<Box display="flex" flexDirection="column">
				<Typography variant="h6" fontWeight={'bold'}>
					{title}
				</Typography>
			</Box>
			<Box>
				<Chip
					variant="outlined"
					label={dayjs(date).format('DD MMMM YYYY').toString()}
				/>
			</Box>
		</Box>
	);
};

TaskHeader.propTypes = {
	title: PropTypes.string,
	date: PropTypes.instanceOf(Date),
};

export default TaskHeader;
