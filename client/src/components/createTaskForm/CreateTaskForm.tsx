import { FC, ReactElement, useState, useEffect, useContext } from 'react';
import {
	Box,
	Typography,
	Stack,
	LinearProgress,
	Button,
	Alert,
	AlertTitle,
} from '@mui/material';
import TaskTitleField from './_taskTitleField';
import TaskDescriptionField from './_taskDescriptionField';
import TaskDateField from './_taskDateField';
import TaskSelectField from './_taskSelectField';
import { Status } from './enums/Status';
import { Priority } from './enums/Priority';
import { useMutation } from '@tanstack/react-query';
import { sendApiRequest } from '../../helpers/sendApiRequest';
import { ICreateTask } from '../taskArea/interfaces/ICreateTask';
import { TaskStatusChangedContext } from '../../context';
import dayjs from 'dayjs';
import 'dayjs/locale/tr';
dayjs.locale('tr');

const CreateTaskForm: FC = (): ReactElement => {
	const [title, setTitle] = useState<string | undefined>(undefined);

	const [description, setDescription] = useState<string | undefined>(
		undefined,
	);

	const [date, setDate] = useState<dayjs.Dayjs | null>(dayjs(new Date()));

	const [status, setStatus] = useState<Status>(Status.todo);
	const [priority, setPriority] = useState<Priority>(Priority.low);
	const [showSuccess, setShowSuccess] = useState<boolean>(false);

	const tasksUpdatedContext = useContext(TaskStatusChangedContext);

	const createTaskMutation = useMutation((data: ICreateTask) => {
		return sendApiRequest('/api', 'POST', data);
	});

	const createTaskHandler = () => {
		if (!title || !description || !date || !status || !priority) return;

		const task: ICreateTask = {
			title,
			description,
			date: date.format('YYYY-MM-DD'),
			status,
			priority,
		};

		createTaskMutation.mutate(task);
	};

	useEffect(() => {
		if (createTaskMutation.isSuccess) {
			tasksUpdatedContext.toggle();
			setShowSuccess(true);
		}
		const successTimeout = setTimeout(() => {
			setShowSuccess(false);
		}, 7000);

		return () => {
			clearTimeout(successTimeout);
		};
	}, [createTaskMutation.isSuccess]);

	return (
		<Box
			display={'flex'}
			flexDirection={'column'}
			alignItems={'flex-start'}
			width={'100%'}
			px={4}
			my={6}
		>
			{showSuccess && (
				<Alert
					severity="success"
					sx={{ width: '100%', marginBottom: '16px' }}
				>
					<AlertTitle>Başarılı</AlertTitle>
					<strong>Yeni bir iş oluşturuldu!</strong>
				</Alert>
			)}

			<Typography
				mb={2}
				component={'h2'}
				variant="h6"
				color={'text.primary'}
			>
				Yapılacak İş Oluştur
			</Typography>

			<Stack sx={{ width: '100%' }} spacing={2}>
				<TaskTitleField
					onChange={(e) => setTitle(e.target.value)}
					disabled={createTaskMutation.isLoading}
				/>
				<TaskDescriptionField
					onChange={(e) => setDescription(e.target.value)}
					disabled={createTaskMutation.isLoading}
				/>
				<TaskDateField
					value={date?.toDate()}
					onChange={(date) => setDate(date)}
					disabled={createTaskMutation.isLoading}
				/>
				<Stack sx={{ width: '100%' }} direction={'row'} spacing={2}>
					<TaskSelectField
						label="Durum"
						name="status"
						value={status}
						onChange={(e) => setStatus(e.target.value as Status)}
						disabled={createTaskMutation.isLoading}
						items={[
							{ value: Status.todo, label: Status.todo.toUpperCase() },
							{
								value: Status.inProgress,
								label: Status.inProgress.toUpperCase(),
							},
							{
								value: Status.completed,
								label: Status.completed.toUpperCase(),
							},
						]}
					/>
					<TaskSelectField
						label="Öncelik"
						name="priority"
						value={priority}
						onChange={(e) => setPriority(e.target.value as Priority)}
						disabled={createTaskMutation.isLoading}
						items={[
							{
								value: Priority.high,
								label: Priority.high.toUpperCase(),
							},
							{
								value: Priority.medium,
								label: Priority.medium.toUpperCase(),
							},
							{
								value: Priority.low,
								label: Priority.low.toUpperCase(),
							},
						]}
					/>
				</Stack>
				{createTaskMutation.isLoading && <LinearProgress />}
				<Button
					onClick={createTaskHandler}
					variant="contained"
					size="large"
					fullWidth
					disabled={
						!title ||
						!description ||
						!date ||
						!status ||
						!priority ||
						createTaskMutation.isLoading
					}
				>
					Create A Task
				</Button>
			</Stack>
		</Box>
	);
};

export default CreateTaskForm;
