import { IDisable } from './IDisable';
import { SelectChangeEvent } from '@mui/material/Select';

export interface ISelectItems extends IDisable {
	value: string;
	label: string;
}

export interface ISelectField extends IDisable {
	name?: string;
	label?: string;
	value?: string;
	onChange?: (event: SelectChangeEvent) => void;
	items?: ISelectItems[];
}
