import { IDisable } from './IDisable';
import React from 'react';

export interface ITextField extends IDisable {
	onChange?: (
		event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
	) => void;
}
