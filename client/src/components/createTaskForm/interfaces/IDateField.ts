import { IDisable } from './IDisable';
import dayjs from 'dayjs';

export interface IDateField extends IDisable {
	value?: Date | null;
	onChange?: (date: dayjs.Dayjs | null) => void;
}
