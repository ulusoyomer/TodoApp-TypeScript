import { FC, ReactElement } from 'react';
import { LocalizationProvider, DatePicker } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import dayjs from 'dayjs';
import 'dayjs/locale/tr';
import { IDateField } from './interfaces/IDateField';
import PropTypes from 'prop-types';

const TaskDateField: FC<IDateField> = (props): ReactElement => {
	const {
		value = dayjs(new Date()).toDate(),
		disabled = false,
		onChange = (date) => console.log(date),
	} = props;
	return (
		<LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="tr">
			<DatePicker
				label="Tarih"
				value={dayjs(value)}
				disabled={disabled}
				onChange={onChange}
			/>
		</LocalizationProvider>
	);
};

TaskDateField.propTypes = {
	value: PropTypes.instanceOf(Date),
	disabled: PropTypes.bool,
	onChange: PropTypes.func,
};

export default TaskDateField;
