export enum Status {
	todo = 'Yapılmadı',
	inProgress = 'Yapım Aşamasında',
	completed = 'Tamamlandı',
}
