import { FC, ReactElement, useState } from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { ISelectField } from './interfaces/ISelectField';
import PropTypes from 'prop-types';

const TaskSelectField: FC<ISelectField> = (props): ReactElement => {
	const {
		label = 'Durum',
		name = 'Durum',
		value = '',
		items = [{ value: '0', label: 'Add' }],
		onChange = (e) => {
			console.log(e);
		},
		disabled = false,
	} = props;

	return (
		<FormControl fullWidth size="small">
			<InputLabel id={`${name}-id`}>{label}</InputLabel>
			<Select
				labelId={`${name}-id`}
				id={`${name}-id-select`}
				name="status"
				value={value}
				label={label}
				onChange={onChange}
				disabled={disabled}
			>
				{items.map((item, index) => {
					return (
						<MenuItem key={index} value={item.value}>
							{item.label}
						</MenuItem>
					);
				})}
			</Select>
		</FormControl>
	);
};

TaskSelectField.propTypes = {
	label: PropTypes.string,
	name: PropTypes.string,
	value: PropTypes.string,
	items: PropTypes.arrayOf(
		PropTypes.shape({
			value: PropTypes.string.isRequired,
			label: PropTypes.string.isRequired,
		}).isRequired,
	),
	onChange: PropTypes.func,
	disabled: PropTypes.bool,
};

export default TaskSelectField;
