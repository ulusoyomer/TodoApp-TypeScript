import { FC, ReactElement } from 'react';
import { TextField } from '@mui/material';
import { ITextField } from './interfaces/ITextField';
import PropTypes from 'prop-types';

const TaskDescriptionField: FC<ITextField> = (props): ReactElement => {
	const { onChange = (e) => console.log(e.target.value), disabled = false } =
		props;
	return (
		<TextField
			id="description"
			label="İş Açıklaması"
			variant="outlined"
			size="small"
			name="description"
			fullWidth
			multiline
			rows={4}
			disabled={disabled}
			onChange={onChange}
		/>
	);
};

TaskDescriptionField.propTypes = {
	onChange: PropTypes.func,
	disabled: PropTypes.bool,
};

export default TaskDescriptionField;
