import express, { Express } from 'express';
import { DataSource } from 'typeorm';
import dotenv from 'dotenv';
import cors from 'cors';
import bodyParser from 'body-parser';
import { Task } from './src/tasks/task.entity';
import { taskRouter } from './src/tasks/task.router';

const app: Express = express();
dotenv.config();

app.use(bodyParser.json());
app.use(cors());

export const AppDataSource = new DataSource({
	type: 'mysql',
	host: process.env.DB_HOST,
	port: Number(process.env.DB_PORT),
	username: process.env.DB_USER,
	password: process.env.DB_PASS,
	database: process.env.DB_NAME,
	entities: [Task],
	synchronize: true,
});

const port = process.env.PORT || 3000;

app.use('/', taskRouter);

AppDataSource.initialize()
	.then(() => {
		console.log('Database connection established');
		app.listen(port, () => {
			console.log(`Server is running on port ${port}`);
		});
	})
	.catch((err) => {
		console.log('Error connecting to database', err);
	});
