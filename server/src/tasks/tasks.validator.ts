import { body, ValidationChain, param } from 'express-validator';
import { Priority } from '../enums/Priority';
import { Status } from '../enums/Status';

export const createValidator: ValidationChain[] = [
	body('title')
		.not()
		.isEmpty()
		.withMessage('Başlık alanı boş bırakılamaz')
		.trim()
		.isString()
		.withMessage('Başlık alanı yazı olmalıdır'),

	body('date')
		.not()
		.isEmpty()
		.withMessage('Tarih alanı boş bırakılamaz')
		.isString()
		.withMessage('Tarih alanı yazı olmalıdır'),

	body('description')
		.trim()
		.isString()
		.withMessage('Açıklama alanı yazı olmalıdır'),

	body('priority')
		.trim()
		.isIn(Object.values(Priority))
		.withMessage('Öncelik alanı geçersiz'),

	body('status')
		.trim()
		.isIn(Object.values(Status))
		.withMessage('Durum alanı geçersiz'),
];

export const updateValidator: ValidationChain[] = [
	param('id')
		.not()
		.isEmpty()
		.withMessage('Id alanı boş bırakılamaz')
		.trim()
		.isString()
		.withMessage('Id alanı uuid olmalıdır'),

	body('status')
		.trim()
		.isIn(Object.values(Status))
		.withMessage('Durum alanı geçersiz'),
];
