import { Router } from 'express';
import { tasksController } from './task.controller';
import { createValidator, updateValidator } from './tasks.validator';

export const taskRouter = Router();

taskRouter
	.route('/tasks')
	.get(tasksController.getAllTasks)
	.post(createValidator, tasksController.createTask);

taskRouter.route('/tasks/:id').patch(updateValidator, tasksController.update);
