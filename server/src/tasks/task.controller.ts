import { AppDataSource } from '../../index';
import { Task } from './task.entity';
import { instanceToPlain, plainToInstance } from 'class-transformer';
import { Request, Response } from 'express';
import { validationResult } from 'express-validator';
import { StatusCodes } from 'http-status-codes';
import { UpdateResult } from 'typeorm';

class TasksController {
	public async getAllTasks(req: Request, res: Response): Promise<Response> {
		try {
			let allTasks: Task[] = await AppDataSource.getRepository(Task).find({
				order: {
					date: 'ASC',
				},
			});

			allTasks = instanceToPlain(allTasks) as Task[];
			return res.status(StatusCodes.OK).json(allTasks);
		} catch (_err) {
			return res
				.status(StatusCodes.INTERNAL_SERVER_ERROR)
				.json({ message: 'Bir hata oluştu' });
		}
	}

	public async createTask(req: Request, res: Response): Promise<Response> {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res
				.status(StatusCodes.BAD_REQUEST)
				.json({ errors: errors.array() });
		}
		const newTask = new Task();
		newTask.title = req.body.title;
		newTask.date = req.body.date;
		newTask.description = req.body.description;
		newTask.priority = req.body.priority;
		newTask.status = req.body.status;
		try {
			let createdTask = await AppDataSource.getRepository(Task).save(
				newTask,
			);
			createdTask = instanceToPlain(createdTask) as Task;
			return res.status(StatusCodes.CREATED).json(createdTask);
		} catch (error) {
			return res
				.status(StatusCodes.INTERNAL_SERVER_ERROR)
				.json({ message: 'Bir hata oluştu' });
		}
	}

	public async update(req: Request, res: Response): Promise<Response> {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res
				.status(StatusCodes.BAD_REQUEST)
				.json({ errors: errors.array() });
		}
		try {
			const task: Task | null = await AppDataSource.getRepository(
				Task,
			).findOne({
				where: {
					id: req.params.id,
				},
			});
			if (!task) {
				return res
					.status(StatusCodes.NOT_FOUND)
					.json({ message: 'Görev bulunamadı' });
			}
			let updatedTask: UpdateResult = await AppDataSource.getRepository(
				Task,
			).update(
				req.params.id,
				plainToInstance(Task, {
					status: req.body.status,
				}),
			);
			updatedTask = instanceToPlain(updatedTask) as UpdateResult;
			return res.status(StatusCodes.OK).json(updatedTask);
		} catch (_err) {
			return res
				.status(StatusCodes.INTERNAL_SERVER_ERROR)
				.json({ message: 'Bir hata oluştu' });
		}
	}
}

export const tasksController = new TasksController();
